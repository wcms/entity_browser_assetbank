/**
 * @file
 */
(function ($) {
    Drupal.behaviors.entity_browser_assetbank = {
        attach: function () {
            var callbackUrl = "?callbackurl=" + window.location.protocol + '//' + window.location.hostname + drupalSettings.path.baseUrl + 'select_image';
            var assetBankURL = $("input[name=assetbank_openUrl]").val() + callbackUrl;
            $(".assetbankSelection").on("click", function(event){
                window.open(assetBankURL, "assetbank", 'width=800, height=1000, location=yes,resizable=no,scrollbars=yes,status=yes, toolbar=no, menubar=no');
                window.addEventListener('message', function (ev) {
                    $("#assetbank_url").attr('value', ev.data);
                    $(".assetbank_url_textfield").css("display", "block");
                    $(".form-actions").removeAttr("hidden");
                });

                event.preventDefault();
            });
        }
    };
}(jQuery));
