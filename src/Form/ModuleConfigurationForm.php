<?php

namespace Drupal\entity_browser_assetbank\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ModuleConfigurationForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_browser_assetbank_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_browser_assetbank.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_browser_assetbank.settings');
    $form['assetbank_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Asset Bank URL'),
      '#default_value' => $config->get('assetbank_url'),
    );

    $form['assetbank_upload_location'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Image upload location'),
      '#default_value' => $config->get('assetbank_upload_location'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = \Drupal::service('config.factory')->getEditable('entity_browser_assetbank.settings');

    $config->set('assetbank_url', $values['assetbank_url']);
    $config->set('assetbank_upload_location', $values['assetbank_upload_location']);
    $config->save();
  }
}
