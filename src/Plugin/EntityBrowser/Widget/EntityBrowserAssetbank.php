<?php

/**
 * @file
 * Custom AssetBank widget for Entity Browser.
 */
namespace Drupal\entity_browser_assetbank\Plugin\EntityBrowser\Widget;

use Drupal\entity_browser\WidgetBase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal;
use Drupal\Core\Url;
use Drupal\Core\File\FileSystemInterface;

/**
 * Reference an AssetBank files using third-party iframe.
 *
 * @EntityBrowserWidget(
 *   id = "entity_browser_assetbank",
 *   label = @Translation("AssetBank file"),
 *   description = @Translation("Reference an Asset Bank files using third-party iframe.")
 * )
 */
class EntityBrowserAssetbank extends WidgetBase {
  /**
   * @var \Drupal\Core\Session\AccountProxyInterface */
  protected $currentUser;

  /**
   * AssetBank constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, ModuleHandlerInterface $module_handler, AccountProxyInterface $current_user, FileSystemInterface $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
    $this->currentUser = $current_user;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'url_field_title' => $this->t('URL to file'),
        'submit_text' => $this->t('Select file'),
        'upload_location' => 'public://',
        'extensions' => 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp',
        'media_type' => NULL,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);
    $field_cardinality = $form_state->get(['entity_browser', 'validators', 'cardinality', 'cardinality']);
    $upload_validators = $form_state->has(['entity_browser', 'widget_context', 'upload_validators']) ? $form_state->get(['entity_browser', 'widget_context', 'upload_validators']) : [];
    $form['browse'] = array(
      '#type' => 'button',
      '#value' => $this->t('Browse'),
      '#executes_submit_callback' => FALSE,
      '#attributes' => array(
        'class' => array('assetbankSelection'),
        'type' => 'button',
      ),
    );
    $form['container']['output'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['edit-output'],
      ],
    ];
    $form['assetbank_url'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="assetbank_url_textfield">',
      '#suffix' => '</div>',
      '#title' => $this->configuration['url_field_title'],
      '#attributes' => array(
        'id' => 'assetbank_url',
      ),
    ];

      $config = Drupal::config('entity_browser_assetbank.settings');
      $form['open_url'] = array(
        '#type' => 'textfield',
        '#maxlength' => 255,
        '#default_value' => $config->get('assetbank_url'),
        '#attributes' => array(
          'class' => array('assetbank_openUrl'),
          'name' => 'assetbank_openUrl',
          'style' => 'display: none;',
        ),
        '#prefix' => '<div class="url-block">',
        '#suffix' => '</div>',
      );
    $form['actions'] = [
      '#type' => 'actions',
      '#attributes' => ['hidden' => 'hidden'],
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->configuration['submit_text'],
        '#eb_widget_main_submit' => TRUE,
        '#button_type' => 'primary',
      ],
    ];
      return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $config = Drupal::config('entity_browser_assetbank.settings');
    $entities = [];
    $image_url = $form_state->getValue('assetbank_url');
    if ($image_url != '') {
      $destination = $config->get('assetbank_upload_location');
      if (!is_dir($destination)) {
        mkdir($destination);
      }

      $file = system_retrieve_file($image_url, $destination, TRUE, FILE_EXISTS_REPLACE);
      $fid = $file->id();

    }
    if($this->configuration['media_type'] == 'image') {

      $values = array(
        'bundle' => 'image',
        'uid' => $this->currentUser->id(),
        'status' => TRUE,
        'field_media_image' => [
          'target_id' => $fid,
        ],

      );
      $media_image = Drupal\media\Entity\Media::create($values);
      $entities[] = $media_image;

    }
    else{
      $file_entity = Drupal\file\Entity\File::create([
        'filename' => $this->fileSystem->basename($file),
        'uri' => $file,
        'uid' => $this->currentUser->id(),
        // This sets the file as permanent.
        'status' => TRUE,
      ]);
      $entities[] = $file_entity;
    }

    $form_state->set('assetbank_prepared', $entities);
      return $entities;

  }


  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    $prepared_entities = $form_state->get('assetbank_prepared', []);
    foreach ($prepared_entities as $key => $prepared_entity) {
      $prepared_entities[$key]->save();
    }

    $this->selectEntities($prepared_entities, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['url_field_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL field title'),
      '#default_value' => $this->configuration['url_field_title'],
      '#required' => TRUE,
    ];
    $form['submit_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit button text'),
      '#default_value' => $this->configuration['submit_text'],
      '#required' => TRUE,
    ];
    $media_type_options = [];
    $media_types = $this
      ->entityTypeManager
      ->getStorage('media_type')
      ->loadByProperties(['source' => 'image']);

    foreach ($media_types as $media_type) {
      $media_type_options[$media_type->id()] = $media_type->label();
    }

    if (empty($media_type_options)) {
      $url = Url::fromRoute('entity.media_type.add_form')->toString();
      $form['media_type'] = [
        '#markup' => $this->t("You don't have media type of the Image type. You should <a href='!link'>create one</a>", ['!link' => $url]),
      ];
    }
    else {
      $form['media_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Media type'),
        '#default_value' => $this->configuration['media_type'],
        '#options' => $media_type_options,
      ];
    }

    return $form;
  }
}
